<?php

/*
 * class custom
 */

class Custom {

    public function __construct() {
        
    }

    /**
     * Get all "Áreas de docente"
     * @global type $DB
     * @return type
     */
    public static function get_areas() {
        global $DB;

        $areas = $DB->get_records_sql("SELECT mdl_course.id, mdl_course.fullname
                    FROM mdl_course
                    JOIN mdl_course_categories ON mdl_course_categories.id = mdl_course.category
                    WHERE mdl_course.category = 4 AND mdl_course.visible = 1");

        return $areas;
    }

    /**
     * create group on a course
     * @global type $DB
     * @param type $course
     */
    public static function create_group_on_course($course, $name, $description) {
        global $DB;

        $data = new stdClass();

        $data->courseid = $course;
        $data->idnumber = "";
        $data->name = $name;
        $data->description = $description;
        $data->descriptionformat = 1;
        $data->enrolmentkey = null;
        $data->picture = 0;
        $data->hidepicture = 1;
        $data->timecreated = strtotime('now');
        $data->timemodified = strtotime('now');

        try {
            $transaction = $DB->start_delegated_transaction();
            $group = $DB->insert_record('groups', $data, $returnid = TRUE);

            $transaction->allow_commit();
        } catch (Exception $e) {
            $transaction->rollback($e);
        }

        return $group;
    }

    /**
     * Get a group by course
     * @global type $DB
     * @param type $course
     * @return array
     */
    public static function get_groups_by_course($course) {
        global $DB;

        $groups = $DB->get_records_sql("SELECT mco_groups.id, mco_groups.name "
                . "FROM mco_groups "
                //. "JOIN mco_groups_members ON mco_groups_members.groupid = mco_groups.id "
                . "WHERE mco_groups.courseid = {$course};");

        return $groups;
    }

    /**
     * insert user into group table
     * @param type $user
     * @param type $group
     */
    public static function insert_member_into_group($user, $group) {
        global $DB;

        $data = new stdClass();

        $data->groupid = $group;
        $data->userid = $user;
        $data->timeadded = strtotime('now');
        $data->component = "";
        $data->itemid = 0;

        try {
            $transaction = $DB->start_delegated_transaction();
            $DB->insert_record('groups_members', $data, $returnid = TRUE);

            $transaction->allow_commit();
        } catch (Exception $e) {
            $transaction->rollback($e);
        }
    }

    
    /**
     * Return role "área de docente id"
     */
    public static function get_rol_aread_id(){
        global $DB;
        
        $area_id = $DB->get_record_sql("SELECT id FROM mdl_role WHERE shortname LIKE '%areadedocente%'");
        
        if($area_id)
            return $area_id;
        else
            return 5; //student
    }
    
    
    /**
     * get context id from role assigment
     * @param type $course
     */
    public static function get_contextid($courses){
        global $DB;
        
        $contexts_id = array();
        
        foreach($courses as $course){
            $context_id = $DB->get_record_sql("SELECT id FROM mdl_context WHERE mdl_context.instanceid = {$course} AND contextlevel = 50;");
            array_push($contexts_id, $context_id);
        }
        
        return $contexts_id;
        
    }
    
    
    /**
     * Assign role ("área de docente")
     * @param type $userid
     */
    public static function assign_rol_docente($userid, $courses) {
        global $DB;
        
        //get area docente id
        $area = self::get_rol_aread_id();
        $contexts = self::get_contextid($courses);
        
        $data = new stdClass();

        foreach ($contexts as $context){
            $data->roleid = $area->id;
            $data->contextid = $context->id;
            $data->userid = $userid;
            $data->timemodified = strtotime('now');
            $data->modifierid = 2;
            $data->component = '';
            $data->itemid = 0;
            $data->sortorder = 0;

            try {
                $transaction = $DB->start_delegated_transaction();
                $DB->insert_record('role_assignments', $data, $returnid = TRUE);

                $transaction->allow_commit();
            } catch (Exception $e) {
                $transaction->rollback($e);
            }
        }
    }
    
    /**
     * is docente
     * @param type $user
     */
    public static function is_docente($user){
        global $DB;

        $is_docente = $DB->get_record_sql("SELECT mdl_user.id "
                . "FROM mdl_role_assignments "
                . "JOIN mdl_user ON mdl_role_assignments.userid = mdl_user.id "
                . "JOIN mdl_role ON mdl_role.id = mdl_role_assignments.roleid "
                . "WHERE mdl_role.shortname LIKE '%areadedocente%' AND mdl_user.id = {$user};");
        
                if($is_docente)
                    return false;
                else
                    return true;
    }

}
