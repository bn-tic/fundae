<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['iomad_reports:addinstance'] = 'Agregar un nuevo bloque de informes';
$string['iomad_reports:myaddinstance'] = 'Agregue un nuevo bloque de Informes Iomad al panel de control de los usuarios';
$string['iomad_reports:view'] = 'Acceso a los informes';
$string['link'] = 'Panel de control';
$string['nocompanyselected'] = 'No se seleccionó ninguna compañía';
$string['pluginname'] = 'Informes';
$string['privacy:metadata'] = 'El bloque selector de la compañía solo muestra datos almacenados en otras ubicaciones.';
