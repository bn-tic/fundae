<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['companies'] = 'Empresas';
$string['completed'] = 'Completado';
$string['course'] = 'Curso';
$string['report_user_logins:view'] = 'Ver el informe de inicio de sesión de usuario';
$string['completiondate'] = 'Fecha de Terminación';
$string['completionnotenabled'] = 'El seguimiento de finalización no está habilitado para este curso (no hay datos de informes disponibles)';
$string['coursechart'] = 'Dibuje un cuadro general para el curso';
$string['iomad_completion:view'] = 'informe de finalización';
$string['error'] = 'Error al encontrar el campo de perfil personalizado para la empresa';
$string['used'] = 'Usado';
$string['name'] = 'Nombre';
$string['nodata'] = 'No hay datos del informe.';
$string['unused'] = 'Sin usar';
$string['nousers'] = 'No hay usuarios en este curso.';
$string['participant'] = 'Participante';
$string['percentage'] = 'Porcentaje completado';
$string['pluginname'] = 'Informe de inicio de sesión del usuario';
$string['privacy:metadata:local_report_user_logins:id'] = 'ID de registro de inicio de sesión de usuario de informe local';
$string['privacy:metadata:local_report_user_logins:userid'] = 'ID Usuario';
$string['privacy:metadata:local_report_user_logins:created'] = 'El usuario creó la marca de tiempo de Unix';
$string['privacy:metadata:local_report_user_logins:firstlogin'] = 'Primera marca de tiempo de inicio de sesión de usuario';
$string['privacy:metadata:local_report_user_logins:lastlogin'] = 'El último usuario ingresó la marca de tiempo de Unix';
$string['privacy:metadata:local_report_user_logins:logincount'] = 'Cuenta de inicio de sesión de usuario';
$string['privacy:metadata:local_report_user_logins'] = 'Datos de usuario de inicio de sesión de usuario de informe local';
$string['repuserlogins'] = 'Informe de inicio de sesión del usuario';
$string['remaining'] = 'Restante';
$string['reportselect'] = 'Filtro de informe';
$string['select'] = '--Seleccione--';
$string['status'] = 'Estado';
$string['trackedcount'] = 'Número de recursos / actividades rastreados: ';
$string['userrecordnotfound'] = 'No se encontró el registro de usuario';
$string['coursename'] = 'Nombre del curso';
$string['completionenabled'] = 'Terminación habilitada';
$string['numallocated'] = 'Licencias asignadas';
$string['notstartedusers'] = 'No empezado';
$string['inprogressusers'] = 'Todavía en progreso';
$string['completedusers'] = 'Completado';
$string['completionisenabled'] = 'Si';
$string['certificate'] = 'Certificado';
$string['coursesummary'] = 'Resumen del curso';
$string['courseusers'] = 'Resumen del usuario para el curso - ';
$string['percent'] = '% completado';
$string['timestarted'] = 'Fecha iniciada';
$string['timecompleted'] = 'Fecha completada';
$string['finalscore'] = 'Puntuación final';
$string['downloadcsv'] = 'Descargar como archivo CSV';
$string['invalidcourse'] = 'La ID del curso aprobada no es válida para esta empresa / departamento';
$string['firstname'] = 'Nombre';
$string['lastname'] = 'Apellido';
$string['email'] = 'Email';
$string['timeenrolled'] = 'Fecha de matriculación';
$string['dateenrolled'] = 'Fecha de matriculación';
$string['datestarted'] = 'Fecha iniciada';
$string['datecompleted'] = 'Fecha completada';
$string['department'] = 'Departamento';
$string['completionfrom'] = 'Fecha de inicio de finalización';
$string['completionto'] = 'Fecha de finalización';
$string['started'] = 'Iniciado';
$string['summarychart'] = 'Dibuje un cuadro resumen para todos los cursos.';
$string['usersummary'] = 'Resumen del curso';
$string['cchart'] = 'Gráfico';
$string['reportallusers'] = 'Todos los usuarios del departamento';
$string['allusers'] = 'Mostrar todos los usuarios';
$string['licensename'] = 'Nombre de licencia';
$string['isusing'] = 'En uso';
$string['issuedate'] = 'Fecha asignada';
$string['licensesendreminder'] = '¿Desea enviar un recordatorio por correo electrónico a esta lista de usuarios? ¿Desea enviar un recordatorio por correo electrónico a esta lista de usuarios?';
$string['sendreminderemail'] = 'Enviar un recordatorio por correo electrónico';
$string['licenseemailsent'] = 'Correos electrónicos han sido enviados';
$string['userssummary'] = '{$a->usercount} Usuarios ({$a->totalcreations} Creados - {$a->totallogins} Log ins)';
