<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['actions'] = 'Comportamiento';
$string['allocated'] = 'Asignado: {$a}';
$string['blocktitle'] = 'Informe de asignaciones de licencia de usuario';
$string['certificate'] = 'Certificado';
$string['clear'] = 'Clear';
$string['clearconfirm'] = 'El usuario será eliminado del curso y todos sus datos serán eliminados. Todavía tendrán una licencia para reiniciar el curso. ¿Seguro que quieres hacer esto?';
$string['completed'] = 'Completado';
$string['coursedetails'] = 'Informe completo del curso';
$string['dateallocated'] = 'Fecha asignada';
$string['datecompleted'] = 'Fecha completada';
$string['datestarted'] = 'Curso asignado / iniciado';
$string['dateunallocated'] = 'Fecha sin asignar';
$string['detail'] = 'Detalle';
$string['deleteconfirm'] = 'El usuario será eliminado del curso, se eliminarán todos sus datos y se reasignarán las licencias. Esto no se puede deshacer. ¿Estás seguro?';
$string['department'] = 'Departamento';
$string['downloadcert'] = 'Ver certificado como PDF';
$string['inprogress'] = 'En progreso';
$string['licenseallocated'] = 'Licencia asignada';
$string['nocerttodownload'] = 'Certificado no alcanzado';
$string['nofurtherdetail'] = 'No hay más detalles para mostrar';
$string['notstarted'] = 'No empezado';
$string['pluginname'] = 'Informe de asignaciones de licencia de usuario';
$string['privacy:metadata:local_report_user_lic_allocs:id'] = 'ID de registro de asignación de licencia de usuario de informe local';
$string['privacy:metadata:local_report_user_lic_allocs:courseid'] = 'ID curso';
$string['privacy:metadata:local_report_user_lic_allocs:action'] = 'Acción de asignación';
$string['privacy:metadata:local_report_user_lic_allocs:userid'] = 'ID usuario';
$string['privacy:metadata:local_report_user_lic_allocs:licenseid'] = 'ID Licencia';
$string['privacy:metadata:local_report_user_lic_allocs:issuedate'] = 'Problema de licencia marca de tiempo de unix';
$string['privacy:metadata:local_report_user_lic_allocs'] = 'Información de usuario de asignación de licencia de usuario de informe local';
$string['report_user_license_allocations_title'] = 'Informe de asignaciones de licencia de usuario';
$string['report_user_license_allocations:view'] = 'Ver el informe de asignaciones de licencias de usuario';
$string['repuserlicallocation'] = 'Informe de asignaciones de licencia de usuario';
$string['scormattempts'] = 'Número de intentos';
$string['scormnotstarted'] = 'Usuario inscrito en el curso. Módulo no iniciado';
$string['scormquestion'] = 'ID pregunta';
$string['scormresult'] = 'Resultado';
$string['scormresults'] = 'Resultado';
$string['scormscore'] = 'Puntuación';
$string['scormtimestarted'] = 'Empezado en';
$string['scormtype'] = 'Tipo de pregunta';
$string['totalallocate'] = 'Número de asignaciones';
$string['totalunallocate'] = 'Número de desasignaciones';
$string['unallocated'] = 'Sin asignar: {$a}';
$string['user_detail_title'] = 'Informes de usuario';
$string['usercoursedetails'] = 'Detalles de usuario';
$string['userdetails'] = 'Informar información para ';
$string['viewfullcourse'] = 'Ver resumen completo del curso';
