<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['report_emails:view'] = 'Ver el informe de correo electrónico de Iomad';
$string['report_emails:resend'] = 'Reenviar correos electrónicos de Iomad';
$string['pluginname'] = 'Informe de correo electrónico saliente';
$string['privacy:metadata'] = 'El informe de correo electrónico de Local Iomad solo muestra datos almacenados en otras ubicaciones.';
$string['sender'] = 'Remitente';
$string['created'] = 'Fecha de creacion';
$string['due'] = 'Fecha de vencimiento';
$string['sent'] = 'Fecha de envío';
$string['controls'] = 'Controles';
$string['resend'] = 'Reenviar';
$string['resendemail'] = 'Reenviar email';
$string['resendemailfull'] = '¿Estás seguro de que quieres volver a procesar este correo electrónico?';
$string['resendall'] = 'Reenviar todo';
$string['resendallemails'] = 'Reenviar todos los correos electrónicos';
$string['resendallemailsfull'] = '¿Está seguro de que desea reenviar todos los correos electrónicos en la selección actual? Esto podría provocar que se envíen muchos correos electrónicos.'; 
