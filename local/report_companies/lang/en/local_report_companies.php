<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['report_companies:view'] = 'Ver informe de empresas';
$string['childcompany'] = 'Esta es una subempresa. La empresa padre es \'{$a}\'';
$string['companies'] = 'Empresas';
$string['completed'] = 'Completado';
$string['course'] = 'Curso';
$string['coursemanagers'] = 'Managers Empresa';
$string['courseusers'] = 'Usuarios de la empresa';
$string['departmentmanagers'] = 'Administradores de departamento';
$string['iomad_companies:view'] = 'Informe de empresas';
$string['nomanagers'] = 'No hay managers especificados para esta empresa';
$string['nousers'] = 'No hay usuarios especificados para esta empresa.';
$string['overview'] = 'Visión general';
$string['pluginname'] = 'Informe general de la compañía';
$string['privacy:metadata'] = 'El informe general de la compañía Local solo muestra datos almacenados en otras ubicaciones.';
$string['totalusercount'] = 'Número total de usuarios : {$a}';
$string['totalcoursecount'] = 'Número total de cursos : {$a}';
$string['courses'] = 'Cursos de la empresa';
$string['nocourses'] = 'No hay cursos especificados para esta empresa.';
$string['completionreportlink'] = 'Información de finalización';
$string['themeinfo'] = 'Tema empresa';
$string['themedetails'] = 'Detalles de tema : ';
$string['notheme'] = 'Tema no definido';
