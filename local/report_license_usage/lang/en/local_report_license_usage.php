<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['blocktitle'] = 'Informe de asignaciones de licencia';
$string['pluginname'] = 'Informe de asignaciones de licencia';
$string['privacy:metadata'] = 'El informe de asignaciones de licencias de Local Iomad solo muestra datos almacenados en otras ubicaciones.';
$string['report_license_usage_title'] = 'Informe de asignaciones de licencia';
$string['report_license_usage:view'] = 'Ver el informe de asignaciones de licencias';
$string['repuserlicallocation'] = 'Informe de asignaciones de licencia';
$string['totalallocate'] = 'Número de asignaciones';
$string['totalunallocate'] = 'Número de asignaciones';
$string['numstart'] = 'Previamente asignado';
$string['numnet'] = 'Asignaciones netas';
$string['numtotal'] = 'Total asignado';
