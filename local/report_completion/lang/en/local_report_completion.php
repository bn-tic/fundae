<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['companies'] = 'Empresas';
$string['completed'] = 'Completado';
$string['course'] = 'Curso';
$string['report_completion:view'] = 'Ver el informe de progreso del curso';
$string['completiondate'] = 'Fecha de Terminación';
$string['completionnotenabled'] = 'El seguimiento de finalización no está habilitado para este curso (no hay datos de informes disponibles)';
$string['coursechart'] = 'Dibuje un cuadro general para el curso';
$string['iomad_completion:view'] = 'Informe de finalización';
$string['error'] = 'Error al encontrar el campo de perfil personalizado para la empresa';
$string['inprogress'] = 'En progreso';
$string['name'] = 'Nombre';
$string['nodata'] = 'No hay datos del informe.';
$string['notstarted'] = 'No empezado';
$string['nousers'] = 'No hay usuarios en este curso.';
$string['participant'] = 'Participante';
$string['percentage'] = 'Porcentaje completado';
$string['pluginname'] = 'Informe final por curso';
$string['privacy:metadata'] = 'El informe de finalización del curso solo muestra los datos almacenados en otras ubicaciones.';
$string['repcoursecompletion'] = 'Informe final por curso';
$string['remaining'] = 'Restante';
$string['reportselect'] = 'Filtro de informe';
$string['select'] = '--Seleccione--';
$string['status'] = 'Estado';
$string['trackedcount'] = 'Número de recursos / actividades rastreados: ';
$string['userrecordnotfound'] = 'No se encontró el registro de usuario';
$string['coursename'] = 'Nombre del curso';
$string['completionenabled'] = 'Finalización habilitada';
$string['numusers'] = 'Número de usuarios';
$string['notstartedusers'] = 'Sin empezar';
$string['inprogressusers'] = 'Todavía en progreso';
$string['completedusers'] = 'Completado';
$string['completionisenabled'] = 'Si';
$string['certificate'] = 'Certificado';
$string['coursesummary'] = 'Resumen del curso';
$string['courseusers'] = 'Resumen del usuario para el curso - ';
$string['percent'] = '% completado';
$string['timecreated'] = 'Fecha creación';
$string['timestarted'] = 'Fecha de inicio';
$string['timecompleted'] = 'Fecha completado';
$string['finalscore'] = 'Puntuación final';
$string['downloadcsv'] = 'Descargar como archivo CSV';
$string['invalidcourse'] = 'La ID del curso aprobada no es válida para esta empresa / departamento';
$string['firstname'] = 'Nombre';
$string['lastname'] = 'Apellido';
$string['email'] = 'Email';
$string['timeenrolled'] = 'Fecha de matriculación';
$string['dateenrolled'] = 'Fecha de matriculación';
$string['datestarted'] = 'Fecha inicio';
$string['datecompleted'] = 'Fecha completada';
$string['department'] = 'Departamento';
$string['completionfrom'] = 'Fecha de inicio de finalización';
$string['completionto'] = 'Fecha de finalización';
$string['started'] = 'Empezado';
$string['summarychart'] = 'Dibuje un cuadro resumen para todos los cursos.';
$string['usersummary'] = 'Resumen del curso';
$string['cchart'] = 'Gráfico';
$string['reportallusers'] = 'Todos los usuarios del departamento';
$string['allusers'] = 'Mostrar todos los usuarios';
$string['hidehistoricusers'] = 'Eliminar resultados históricos';
$string['historicusers'] = 'Incluir resultados históricos';
$string['historiccompletedusers'] = 'Histórico completado';
$string['timeexpires'] = 'Válido hasta';
$string['showsuspendedusers'] = 'Incluir usuarios suspendidos';
$string['hidesuspendedusers'] = 'Eliminar usuarios suspendidos';
$string['compfrom'] = 'Fecha de inicio';
$string['compto'] = 'Fecha final';
$string['userlink'] = 'Mostrar usuarios';
$string['used'] = 'Activo';
$string['unused'] = 'Inactivo';
$string['notapplicable'] = 'N/A';
