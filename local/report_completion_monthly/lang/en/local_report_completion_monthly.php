<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['blocktitle'] = 'Informe final por mes';
$string['pluginname'] = 'Informe final por mes';
$string['privacy:metadata'] = 'El informe de finalización local por mes solo muestra los datos almacenados en otras ubicaciones.';
$string['report_completion_monthly_title'] = 'Informe final por mes';
$string['report_completion_monthly:view'] = 'Ver el informe de finalización por mes';
$string['repuserlicallocation'] = 'Informe final por mes';
$string['totalallocate'] = 'Número de finalizaciones';
$string['numtotal'] = 'Total finalizaciones';
