<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['actions'] = 'Actions';
$string['blocktitle'] = 'Informe usuario';
$string['certificate'] = 'Certificado';
$string['clear'] = 'Clear';
$string['clearconfirm'] = 'El usuario será eliminado del curso y todos sus datos serán eliminados. Todavía tendrán una licencia para reiniciar el curso. ¿Seguro que quieres hacer esto?';
$string['clear_successful'] = 'El usuario ha sido eliminado del curso con éxito.';
$string['completed'] = 'Completado';
$string['coursedetails'] = 'Informe completo del curso';
$string['datecompleted'] = 'Fecha completada';
$string['datestarted'] = 'Curso asignado / iniciado';
$string['delete'] = 'Eliminar';
$string['deleteconfirm'] = 'El usuario será eliminado del curso, se eliminarán todos sus datos y se reasignarán las licencias. Esto no se puede deshacer. ¿Estás seguro?';
$string['delete_successful'] = 'El usuario ha sido eliminado del curso con éxito y su licencia ha sido revocada.';
$string['department'] = 'Departamento';
$string['downloadcert'] = 'Ver certificado como PDF';
$string['inprogress'] = 'En progreso';
$string['nocerttodownload'] = 'Certificado no alcanzado';
$string['nofurtherdetail'] = 'No hay más detalles para mostrar';
$string['notstarted'] = 'No empezado';
$string['pluginname'] = 'Informe de los usuarios';
$string['privacy:metadata'] = 'El informe de finalización de usuario de Local Iomad solo muestra datos almacenados en otras ubicaciones.';
$string['report_users_title'] = 'Informe de usuario';
$string['report_users:view'] = 'Ver el informe de los usuarios';
$string['report_users:deleteentries'] = 'Eliminar o borrar la información de los usuarios de un curso.';
$string['repusercompletion'] = 'Informe final por usuario';
$string['scormattempts'] = 'Número de intentos';
$string['scormnotstarted'] = 'Usuario inscrito en el curso. Módulo no iniciado';
$string['scormquestion'] = 'ID pregunta';
$string['scormresult'] = 'Resultado';
$string['scormresults'] = 'Resultado';
$string['scormscore'] = 'Puentuación';
$string['scormtimestarted'] = 'Comenzado en';
$string['scormtype'] = 'Tipo de pregunta';
$string['user_detail_title'] = 'Informes de usuario';
$string['usercoursedetails'] = 'Detalles de usuario';
$string['userdetails'] = 'Informar información para ';
$string['viewfullcourse'] = 'Ver resumen completo del curso';
