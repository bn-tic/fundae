<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_iomad', language 'en'
 */

$string['cannotemailnontemporarypasswords'] = 'No es seguro enviar contraseñas por correo electrónico sin obligarlas a cambiarlas en el primer inicio de sesión.';
$string['companycityfilter'] = 'La ubicación de la compañía contiene';
$string['companycountryfilter'] = 'El país de la compañía contiene';
$string['companycourses'] = 'Otros cursos de empresa';
$string['companyfilter'] = 'Filtrar Resultados';
$string['companynamefilter'] = 'El nombre de la compañía contiene';
$string['companysearchfields'] = 'Campos de búsqueda de empresas';
$string['crontask'] = 'Cron';
$string['emailfilter'] = 'Dirección de correo electrónico contiene';
$string['firstnamefilter'] = 'El primer nombre contiene';
$string['iomad'] = 'Panel de control';
$string['lastnamefilter'] = 'El apellido contiene';
$string['missingaccesstocourse'] = 'No tienes permiso para eso.';
$string['nopermissions'] = 'El administrador no le ha dado permiso para hacer esto.';
$string['pluginname'] = 'Panel de control';
$string['privacy:metadata'] = 'El complemento Local solo muestra datos almacenados en otras ubicaciones.';
$string['privacy:metadata:company_users:companyid'] = 'Identificación de la compañía de los usuarios de la compañía';
$string['privacy:metadata:company_users:userid'] = 'ID de usuario de usuarios de la empresa';
$string['privacy:metadata:company_users:managertype'] = 'Tipo de administrador de usuarios de la empresa';
$string['privacy:metadata:company_users:departmentid'] = 'ID del departamento de usuarios de la empresa';
$string['privacy:metadata:company_users:suspended'] = 'Los usuarios de la empresa suspendieron bandera';
$string['privacy:metadata:company_users'] = 'Usuarios empresa';
$string['privacy:metadata:companylicense_users:licenseid'] = 'ID de licencia de usuarios de licencia de empresa';
$string['privacy:metadata:companylicense_users:userid'] = 'ID de usuario de usuarios de licencia de empresa';
$string['privacy:metadata:companylicense_users:isusing'] = 'Los usuarios de la licencia de la empresa usan la bandera';
$string['privacy:metadata:companylicense_users:timecompleted'] = 'Licencia de la empresa usuarios tiempo completado';
$string['privacy:metadata:companylicense_users:score'] = 'Puntuación de usuarios de licencia de empresa';
$string['privacy:metadata:companylicense_users:result'] = 'Resultado de usuarios de licencia de empresa';
$string['privacy:metadata:companylicense_users:licensecourseid'] = 'ID de curso de licencia de usuarios de licencia de empresa';
$string['privacy:metadata:companylicense_users:issuedate'] = 'Fecha de emisión del usuario de la licencia de la empresa';
$string['privacy:metadata:companylicense_users:groupid'] = 'ID de grupo de usuarios de licencia de empresa';
$string['privacy:metadata:companylicense_users'] = 'Usuarios de licencia de la empresa';
$string['removelicenses'] = 'Eliminado: registros y licencias de cursos de la empresa';
$string['setupiomad'] = 'Comience a configurar panel de control';
$string['show_suspended_users'] = '¿Mostrar usuarios suspendidos?';
$string['userfilter'] = 'Filtrar Resultados';
$string['usersearchfields'] = 'Campos de busqueda de usuario';
$string['show_suspended_companies'] = '¿Mostrar empresas suspendidas?';
