<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['attendance'] = 'Asistentes';
$string['companies'] = 'Empresas';
$string['completed'] = 'Completado';
$string['course'] = 'Curso';
$string['report_attendance:view'] = 'Ver el informe de asistencia al curso';
$string['completiondate'] = 'Fecha de Terminación';
$string['completionnotenabled'] = 'El seguimiento de finalización no está habilitado para este curso (no hay datos de informes disponibles)';
$string['iomad_attendance:view'] = 'informe de asistencia';
$string['error'] = 'Error al encontrar el campo de perfil personalizado para la empresa';
$string['event'] = 'Evento de entrenamiento -';
$string['inprogress'] = 'En progreso';
$string['name'] = 'Nombre';
$string['nodata'] = 'No hay datos del informe.';
$string['notstarted'] = 'No empezado';
$string['nousers'] = 'No hay usuarios en este curso.';
$string['participant'] = 'Participante';
$string['percentage'] = 'Porcentaje completo';
$string['pluginname'] = 'Informe de asistencia por curso';
$string['privacy:metadata'] = 'El informe de asistencia de Iomad local solo muestra datos almacenados en otras ubicaciones.';
$string['repcourseattendance'] = 'Informe de asistencia por curso';
$string['remaining'] = 'Restante';
$string['reportselect'] = 'Filtro de informe';
$string['select'] = '--Seleccione--';
$string['status'] = 'Estado';
$string['trackedcount'] = 'Número de recursos / actividades rastreados:';
$string['userrecordnotfound'] = 'No se encontró el registro de usuario';
$string['coursename'] = 'Nombre del curso';
$string['completionenabled'] = 'Terminación habilitada';
$string['numusers'] = 'Numero de usuarios';
$string['nocourses'] = '<h2> No hay eventos de entrenamiento </h2>
<p> No hay actividades de eventos de capacitación configuradas en ninguno de sus cursos. </p>';
$string['notstartedusers'] = 'No empezado';
$string['inprogressusers'] = 'Todavía en progreso';
$string['completedusers'] = 'Completdo';
$string['completionisenabled'] = 'Si';
$string['certificate'] = 'Certificado';
$string['coursesummary'] = 'Resumen del curso';
$string['courseusers'] = 'Resumen del usuario para el curso - ';
$string['percent'] = '% completado';
$string['datestarted'] = 'Fecha asignada / iniciada';
$string['datecompleted'] = 'Fecha completada';
$string['finalscore'] = 'Puntuación final';
$string['downloadcsv'] = 'Descargar como archivo CSV';
$string['invalidcourse'] = 'La ID del curso aprobada no es válida para esta empresa / departamento';
